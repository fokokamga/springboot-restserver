package com.thales.springbootrestserverapi.dao;

import com.thales.springbootrestserverapi.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByLogin(String login);
}
