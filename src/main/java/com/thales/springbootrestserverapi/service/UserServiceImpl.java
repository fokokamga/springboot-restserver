package com.thales.springbootrestserverapi.service;

import com.thales.springbootrestserverapi.dao.UserRepository;
import com.thales.springbootrestserverapi.model.User;
import org.apache.commons.collections4.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import java.util.Collection;

@Service(value = "userService")// l'annotation @Service est optionnelle ici, car il n'existe qu'une seule implémentation de l'interface UserService
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public User findByLogin(String login) {

        return userRepository.findByLogin(login);
    }

    @Override
    public Collection<User> getAllUsers() {

        return IteratorUtils.toList(userRepository.findAll().iterator());
    }

    @Override
    public User getUserById(Long id) {
        return null;
    }

    //@Override
    @Transactional(readOnly=false)
    public User saveOrUpdateUser() {
        return saveOrUpdateUser();
    }

    //@Override
    @Transactional(readOnly=false)
    public User saveOrUpdateUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    @Override
    @Transactional(readOnly=false)
    public void deleteUser(Long id) {
       /// userRepository.delete(id);

    }

}
