# springboot-restserver

Il s'agit de développer un portail web d'inscription et de connexion en utilisant les services RESTFul côté serveur et Spring RestTemplate côté client.

Le serveur sera développé dans une application à part et le client dans une autre. 